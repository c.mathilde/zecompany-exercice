$(document).ready(function(){ 
    $(document).on("click", ".ajoutclient", function(){
        console.log("ca ajoute");
        let nom = $(".nom").val();
        let prenom = $(".prenom").val();
        let date = $(".date").val();
        if (nom == "" || prenom == "" || date == "") {
            alert("Remplissez tous les champs");
        }
        else {
            ajoutClient(nom, prenom, date);
        }
    })
});

function ajoutClient(nom, prenom, date){
    $.ajax({
        type: "POST",
        url: "https://localhost:8000/client/add",
        data: {"nom": nom,
                "prenom": prenom,
                "date": date},
        success: function(data){
            console.log("ok");
            location = "client.html";
        },
        error: function(data){
            console.log("pas ok");},
    });
}