$(document).ready(function(){ 
    console.log("yo");
    onInit();
    $(document).on("click", ".supprimerauteur", function(){
        let auteur_id = $(this).parent().attr('id');
        supprimerAuteur(auteur_id);
    })
    $(document).on("click", ".modifierauteur", function(){
        console.log("oui");
        let auteur_id = $(this).parent().attr('id');
        addForm(auteur_id);
    })
    $(document).on("click", ".modif", function(){
        let auteur_id = $(this).parent().parent().attr('id');
        let nom = $(".nom").val();
        let prenom = $(".prenom").val();
        modifierAuteur(nom, prenom, auteur_id);
    })
    $(document).on("click", ".annuler", function(){
        removeModification();
    })
    $(document).on("click", "#afficherlivres", function(){
        $("#livres").remove();
        let auteur_id = $(this).parent().attr('id');
        getLivres(auteur_id);
    })
});

function onInit() {
    $(".auteurs").empty();
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/auteur/",
        success: function(data){
            auteurs = JSON.parse(data);
            for (auteur of auteurs) {
                $(".auteurs").append(`<div id='`+auteur["id"]+`'>
                <a id="afficherlivres">`+auteur["nom"]+" "+auteur["prenom"]+"</a> <a class='modifierauteur'>Modifier</a> <a class='supprimerauteur'>Supprimer</a>"+`</div>`);
            }
            $(".auteurs").append(`<div><a href="ajoutauteur.html">Ajouter un auteur</a></div>`);
            },
        error: function(data){
            console.log("pas ok");},
    });
}

function supprimerAuteur(id_auteur) {
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/auteur/delete/"+id_auteur,
        success: function(data){
            console.log(data);
            $("#"+id_auteur).remove();
        },
        error: function(data){
            console.log("non");},
    });
}

function addForm(id_auteur) {
    $("#"+id_auteur).append(`<div id="modification">
        <p>Modifier</p>
        <label for="nom">Nom : </label>
            <input type="text" name="nom" class="nom" placeholder="Nom"><br>
        <label for="prenom">Prénom : </label>
            <input type="text" name="prenom" class="prenom" placeholder="Prénom"><br>
        <button class="modif">Modifier</button> <button class="annuler">Annuler</button>
    </div>
    `);
}

function modifierAuteur(nom, prenom, id_auteur){
    $.ajax({
        type: "POST",
        url: "https://localhost:8000/auteur/update/"+id_auteur,
        data: {"nom": nom,
                "prenom": prenom},
        success: function(data){
            console.log("ok");
            onInit();
        },
        error: function(data){
            console.log("pas ok");},
    });
}

function removeModification() {
    $("#modification").remove();
}

function getLivres(auteur_id){
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/livre/auteur/"+auteur_id,
        success: function(data){
            afficherLivre(data, auteur_id);
        },
        error: function(data){
            console.log("pas ok");},
    });
}

function afficherLivre(data, id_auteur){
    if (data == "aucun livre") {
        $("#"+id_auteur).append("<div id='livres'>Aucun livre</div>");
    }
    else {
        let livres = JSON.parse(data);
        $("#"+id_auteur).append(`<div id='livres'><ul></ul></div>`);
        for (livre of livres) {
            $("#livres ul").append(`<li>`+livre+`</li>`);
        }
    }
}