$(document).ready(function(){ 
    onInit();
    $(document).on("click", ".ajoutelivre", function(){
        console.log("ca ajoute");
        let titre = $(".titre").val();
        let auteur = $(".auteur").val();
        console.log(titre+" "+auteur);
        if (titre == "" || auteur == "") {
            alert("Remplissez tous les champs");
        }
        else {
            ajoutLivre(titre, auteur);
        }
    })
});

function onInit() {
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/auteur/",
        success: function(data){
            auteurs = JSON.parse(data);
            for (auteur of auteurs) {
                $(".auteur").append(`
                    <option class="`+auteur["id"]+`" name="`+auteur["id"]+`" value="`+auteur["id"]+`">`+auteur["prenom"]+` `+auteur["nom"]+`</option>
                `)
            }
        },
        error: function(data){
            console.log("pas ok");},
    });
}

function ajoutLivre(titre, auteur){
    $.ajax({
        type: "POST",
        url: "https://localhost:8000/livre/add",
        data: {"titre": titre,
                "auteur": auteur},
        success: function(data){
            console.log("ok");
            location = "livre.html";
        },
        error: function(data){
            console.log("non");},
    });
}