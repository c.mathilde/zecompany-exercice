$(document).ready(function(){ 
    console.log("yo");
    onInit();
    $(document).on("click", ".supprimerclient", function(){
        let client_id = $(this).parent().attr('id');
        supprimerClient(client_id);
    })
    $(document).on("click", ".modifierclient", function(){
        console.log("oui");
        let client_id = $(this).parent().attr('id');
        addForm(client_id);
    })
    $(document).on("click", ".modif", function(){
        let client_id = $(this).parent().parent().attr('id');
        let nom = $(".nom").val();
        let prenom = $(".prenom").val();
        let date = $(".date").val();
        modifierClient(nom, prenom, date, client_id);
    })
    $(document).on("click", ".annuler", function(){
        removeModification();
    })
    $(document).on("click", ".ajouterlivre", function(){
        let client_id = $(this).parent().attr('id');
        createFormBook(client_id);
    })
    $(document).on("click", ".ajoutlivre", function(){
        let client_id = $(this).parent().parent().attr('id');
        let id_livre = $(".livres").val();
        ajoutLivre(client_id, id_livre);
    })
    $(document).on("click", "#afficherlivreslus", function(){
        $("#livreslus").remove();
        let client_id = $(this).parent().attr('id');
        getLivres(client_id);
    })
    $(document).on("click", ".supprimerlivre", function(){
        let client_id = $(this).parent().parent().parent().parent().attr('id');
        let id_livre = $(this).parent().attr("id");
        console.log(client_id);
        console.log(id_livre);
        supprimerLivre(client_id, id_livre);
    })
});

function onInit() {
    $(".clients").empty();
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/client/",
        success: function(data){
            clients = JSON.parse(data);
            for (client of clients) {
                $(".clients").append(`<div id='`+client["id"]+`'>
                <a id="afficherlivreslus">`+client["nom"]+" "+client["prenom"]+"</a> "+client["date"]+` <a class="ajouterlivre">Ajouter un livre</a> <a class='modifierclient'>Modifier</a> <a class='supprimerclient'>Supprimer</a></div>`);
            }
            $(".clients").append(`<div><a href="ajoutclient.html">Ajouter un client</a></div>`);
            },
        error: function(data){
            console.log("pas ok");},
    });
}

function supprimerClient(id_client) {
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/client/delete/"+id_client,
        success: function(data){
            console.log(data);
            $("#"+id_client).remove();
        },
        error: function(data){
            console.log("non");},
    });
}

function addForm(id_client) {
    $("#"+id_client).append(`<div id="modification">
        <p>Modifier</p>
        <label for="nom">Nom : </label>
            <input type="text" name="nom" class="nom" placeholder="Nom"><br>
        <label for="prenom">Prénom : </label>
            <input type="text" name="prenom" class="prenom" placeholder="Prénom"><br>
        <label for="date">Date de naissance : </label>
            <input type="datetime" name="date" class="date" placeholder="Date"><br>
        <button class="modif">Modifier</button> <button class="annuler">Annuler</button>
    </div>
    `);
}

function modifierClient(nom, prenom, date, id_client){
    $.ajax({
        type: "POST",
        url: "https://localhost:8000/client/update/"+id_client,
        data: {"nom": nom,
                "prenom": prenom,
                "date": date},
        success: function(data){
            console.log("ok");
            onInit();
        },
        error: function(data){
            console.log("pas ok");},
    });
}

function removeModification() {
    $("#modification").remove();
}

function createFormBook(id_client) {
    $("#livresajout").remove();
    $("#"+id_client).append(`
        <div id="livresajout">
            <select name="livres" class="livres" id="livres">

            </select>
            <button class="ajoutlivre">Ajouter</button> <button class="annulerlivre">Annuler</button>
        </div>
    `);
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/livre/",
        success: function(data){
            livres = JSON.parse(data);
            for (livre of livres) {
                $(".livres").append(`
                    <option class="`+livre["id"]+`" name="`+livre["id"]+`" value="`+livre["id"]+`">`+livre["titre"]+` `+livre["prenomauteur"]+` `+livre["nomauteur"]+`</option>
                `)
            }
        },
        error: function(data){
            console.log("pas ok");},
    });
}

function ajoutLivre(id_client, id_livre){
    $(".clients").empty();
    $.ajax({
        type: "POST",
        url: "https://localhost:8000/client/read",
        data: {"client": id_client,
                "livre": id_livre},
        success: function(data){
            console.log("ok");
            onInit();
        },
        error: function(data){
            console.log("pas ok");},
    });
}

function getLivres(client_id){
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/client/livre/"+client_id,
        success: function(data){
            afficherLivre(data, client_id);
        },
        error: function(data){
            console.log("pas ok");},
    });
}

function afficherLivre(data, client_id){
    if (data == "aucun livre") {
        $("#"+client_id).append("<div id='livreslus'>Aucun livre</div>");
    }
    else {
        let livres = JSON.parse(data);
        $("#"+client_id).append(`<div id='livreslus'><ul></ul></div>`);
        for (livre of livres) {
            $("#livreslus ul").append(`<li id="`+livre["id"]+`">`+livre["titre"]+" "+livre["prenomauteur"]+" "+livre["nomauteur"]+` <a class="supprimerlivre">Supprimer le livre</a> </li>`);
        }
    }
}

function supprimerLivre(id_client, id_livre) {
    $.ajax({
        type: "POST",
        url: "https://localhost:8000/client/unread/",
        data: {"livre": id_livre,
                "client": id_client},
        success: function(data){
            $("#livreslus ul li#"+id_livre).remove();
        },
        error: function(data){
            console.log("pas ok");},
    });
}