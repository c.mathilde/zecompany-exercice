$(document).ready(function(){ 
    $(document).on("click", ".ajoutauteur", function(){
        console.log("ca ajoute");
        let nom = $(".nom").val();
        let prenom = $(".prenom").val();
        if (nom == "" || prenom == "") {
            alert("Remplissez tous les champs");
        }
        else {
            ajoutAuteur(nom, prenom);
        }
    })
});

function ajoutAuteur(nom, prenom){
    $.ajax({
        type: "POST",
        url: "https://localhost:8000/auteur/add",
        data: {"nom": nom,
                "prenom": prenom},
        success: function(data){
            console.log("ok");
            location = "auteur.html";
        },
        error: function(data){
            console.log("pas ok");},
    });
}