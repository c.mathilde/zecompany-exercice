$(document).ready(function(){ 
    console.log("yo");
    onInit();
    $(document).on("click", ".supprimerlivre", function(){
        let livre_id = $(this).parent().attr('id');
        supprimerLivre(livre_id);
    })
    $(document).on("click", ".modifierlivre", function(){
        console.log("oui");
        let livre_id = $(this).parent().attr('id');
        addForm(livre_id);
    })
    $(document).on("click", ".modif", function(){
        let livre_id = $(this).parent().parent().attr('id');
        let titre = $(".titre").val();
        let auteur = $(".auteur").val();
        modifierLivre(titre, auteur, livre_id);
    })
    $(document).on("click", ".annuler", function(){
        removeModification();
    })
});

function onInit() {
    $(".livres").empty();
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/livre/",
        success: function(data){
            livres = JSON.parse(data);
            for (livre of livres) {
                $(".livres").append(`<div id='`+livre["id"]+`'>
                `+livre["titre"]+" écrit par : <span id='"+livre["idauteur"]+"'>"+livre["nomauteur"]+" "+livre["prenomauteur"]+`</span> <a class='modifierlivre'>Modifier</a> <a class='supprimerlivre'>Supprimer</a></div>`);
            }
            $(".livres").append(`<div><a href="ajoutlivre.html">Ajouter un livre</a></div>`);
        },
        error: function(data){
            console.log("pas ok");},
    });
}

function supprimerLivre(livre_id) {
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/livre/delete/"+livre_id,
        success: function(data){
            console.log(data);
            $("#"+livre_id).remove();
        },
        error: function(data){
            console.log("non");},
    });
}

function addForm(livre_id) {
    $("#modification").remove();
    $("#"+livre_id).append(`<div id="modification">
        <p>Modifier</p>
        <label for="titre">Titre : </label>
            <input type="text" name="titre" class="titre" placeholder="Titre"><br>
        <label for="auteur">Auteur : </label>
            <select name="auteur" class="auteur" id="auteur">

            </select>
        <button class="modif">Modifier</button> <button class="annuler">Annuler</button>
    </div>
    `);
    $.ajax({
        type: "GET",
        url: "https://localhost:8000/auteur/",
        success: function(data){
            auteurs = JSON.parse(data);
            for (auteur of auteurs) {
                $(".auteur").append(`
                    <option class="`+auteur["id"]+`" name="`+auteur["id"]+`" value="`+auteur["id"]+`">`+auteur["prenom"]+` `+auteur["nom"]+`</option>
                `)
            }
        },
        error: function(data){
            console.log("pas ok");},
    });
}

function modifierLivre(titre, auteur, id_livre){
    $.ajax({
        type: "POST",
        url: "https://localhost:8000/livre/update/"+id_livre,
        data: {"titre": titre,
                "auteur": auteur},
        success: function(data){
            console.log("ok");
            onInit();
        },
        error: function(data){
            console.log("pas ok");},
    });
}

function removeModification() {
    $("#modification").remove();
}