<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ClientRepository;
use App\Repository\LivreRepository;
use App\Entity\Client;
use App\Entity\Livre;

/**
 * @Route("/client")
 */
class ClientController extends AbstractController
{
    /**
    * @Route("/", name="client/index")
    */
    public function index(ClientRepository $cr)
    {
        $response = new Response();
        $clients = $cr->findAll();
        $data = [];
        foreach ($clients as $client){
            $subdata = ["id" => $client->getId(),
                        "nom" => $client->getNom(),
                        "prenom" => $client->getPrenom(),
                        "date" => $client->getDateDeNaissance()->format('d-m-Y')
                    ];

            array_push($data, $subdata);
        }
        $response->setContent(json_encode($data));
        return $response;
    }

    /**
    * @Route("/update/{id}", name="client/update", methods="GET|POST")
    */
    public function update(Request $request, ClientRepository $cr, $id)
    {
        $client = $cr->findBy(["id"=>$id])[0];
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod("POST")){
            $prenom = $request->request->get('prenom');
            $nom = $request->request->get('nom');
            $date = $request->request->get('date');
            try{
                if ($date != "") $client->setDateDeNaissance(new \DateTime($date));
            }
            catch(\Exception $e){
                return $this->render('client/update.html.twig', [
                    'client' => $client,
                    "error_message" => "Veuillez rentrer une date valide."
                    ]);
                }
                
                if ($prenom != "") $client->setPrenom($prenom);
                if ($nom != "") $client->setNom($nom);

            $em->persist($client);
            $em->flush();
            

            return new Response("client modifié");
        }
        return new Response("route modification client");
    }

    /**
    * @Route("/delete/{id}", name="client/delete", methods="GET|POST")
    */
    public function delete(Request $request, ClientRepository $cr, $id)
    {
        $client = $cr->findBy(["id"=>$id])[0];
        $em = $this->getDoctrine()->getManager();
        $em->remove($client);
        $em->flush();
        return new Response("client supprimé");
    }

    /**
    * @Route("/add", name="client/add", methods="GET|POST")
    */
    public function add(Request $request, ClientRepository $cr)
    {
        $client = new Client;
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod("POST")){
            $prenom = $request->request->get('prenom');
            $nom = $request->request->get('nom');
            $date = $request->request->get('date');
            try{
                if ($date != "") $client->setDateDeNaissance(new \DateTime($date));
            }
            catch(\Exception $e){
                return $this->render('client/add.html.twig', [
                    'client' => $client,
                    'livres' => $leslivres,
                    "error_message" => "Veuillez rentrer une date valide."
                    ]);
                }
                
                if ($prenom != "") $client->setPrenom($prenom);
                if ($nom != "") $client->setNom($nom);
                
            $em->persist($client);
            $em->flush();
            

            return new Response("client ajouté");
        }
        return new Response("route ajout client");
    }

    /**
    * @Route("/read", name="client/read", methods="GET|POST")
    */
    public function read(Request $request, ClientRepository $cr, LivreRepository $lr)
    {
        $livres=$lr->findAll();
        $clients=$cr->findAll();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod("POST")){
            $idclient = $request->request->get('client');
            $client = $cr->findBy(["id"=>$idclient])[0];
            $livre = $request->request->get('livre');
            if ($livre != "" && $client != "") $client->addLivresLus($lr->find($livre));
            
            $em->persist($client);
            $em->flush();
            
            return new Response("livre ajouté au client");
        }
        return new Response("route ajout livre à client");
    }

    /**
    * @Route("/unread/", name="client/unread", methods="GET|POST")
    */
    public function unread(Request $request, ClientRepository $cr, LivreRepository $lr)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod("POST")) {
            $livre = $request->request->get('livre');
            $client_id = $request->request->get('client');
            if ($livre != "") {
               $client = $cr->find($client_id)->removeLivresLus($lr->find($livre));
            }
            
            $em->persist($client);
            $em->flush();
            
            return $this->redirectToRoute('client/index');
            
        }
    

        return $this->render('client/unread.html.twig', [
            'client' => $client,
            "error_message" => ""
        ]);
    }

    /**
    * @Route("/livre/{id}", name="client/livre", methods="GET")
    */
    public function getLivre(Request $request, ClientRepository $cr,LivreRepository $lr,  $id)
    {
        $response = new Response();
        $client = $cr->findBy(["id"=>$id])[0];
        $livres = $client->getLivresLus();
        if (sizeof($livres) == 0) {
            return new Response("aucun livre");
        }
        else {
            $data = [];
            foreach ($livres as $livre){
                $subdata = ["titre" => $livre->getTitre(),
                            "nomauteur" => $livre->getAuteur()->getNom(),
                            "prenomauteur" => $livre->getAuteur()->getPrenom(),
                            "id" => $livre->getId()
                        ];

                array_push($data, $subdata);
            }
            $response->setContent(json_encode($data));
            return $response;
        }
    }

}