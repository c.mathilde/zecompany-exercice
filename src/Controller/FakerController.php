<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Client;
use App\Entity\Auteur;
use App\Entity\Livre;

class FakerController extends AbstractController {
    /**
     * @Route("/faker", name="faker")
     */
    public function index() {
        $em = $this->getDoctrine()->getManager();

        $client1 = new Client();
        $client1->setNom("Bara")->setPrenom("Alain")->setDateDeNaissance(new \DateTime("1999-02-03"));

        $client2 = new Client();
        $client2->setNom("Chambas")->setPrenom("Mathilde")->setDateDeNaissance(new \DateTime("1999-09-19"));
        
        $auteur1 = new Auteur();
        $auteur1->setNom("Pointereau")->setPrenom("Julien");

        $auteur2 = new Auteur();
        $auteur2->setNom("Rabottin")->setPrenom("Nelson");
        
        $livre1 = new Livre();
        $livre1->setTitre("Titre 1")->setAuteur($auteur1);

        $livre2 = new Livre();
        $livre2->setTitre("Titre 2")->setAuteur($auteur2);

        $client1->addLivresLus($livre1);
        $client2->addLivresLus($livre2);

        $em->persist($client1);
        $em->persist($client2);
        $em->persist($auteur1);
        $em->persist($auteur2);
        $em->persist($livre1);
        $em->persist($livre2);
        $em->flush();


        return new Response('Data created');
    }
}
