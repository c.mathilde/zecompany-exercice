<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\LivreRepository;
use App\Repository\AuteurRepository;
use App\Entity\Livre;
use App\Entity\Auteur;

/**
 * @Route("/livre")
 */
class LivreController extends AbstractController
{
    /**
    * @Route("/", name="livre/index")
    */
    public function index(LivreRepository $lr)
    {
        $response = new Response();
        $livres = $lr->findAll();
        $data = [];
        foreach ($livres as $livre){
            $subdata = ["id" => $livre->getId(),
                        "titre" => $livre->getTitre(),
                        "nomauteur" => $livre->getAuteur()->getNom(),
                        "prenomauteur" => $livre->getAuteur()->getPrenom(),
                        "idauteur" => $livre->getAuteur()->getId()
                    ];

            array_push($data, $subdata);
        }
        $response->setContent(json_encode($data));
        return $response;
    }

    /**
    * @Route("/add", name="livre/add", methods="GET|POST")
    */
    public function add(Request $request, LivreRepository $lr, AuteurRepository $ar)
    {
        $livre = new Livre();
        $lesauteurs = $ar->findAll();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod("POST")){
            $titre = $request->request->get('titre');
            $auteur = $request->request->get('auteur');
                if ($titre != "") $livre->setTitre($titre);
                if ($auteur != "") $livre->setAuteur($ar->find($auteur));
            $em->persist($livre);
            $em->flush();

            return new Response("livre ajouté");
        }
        return new Response("route ajout livre");
    }

    /**
    * @Route("/update/{id}", name="livre/update", methods="GET|POST")
    */
    public function update(Request $request, LivreRepository $lr, AuteurRepository $ar, $id)
    {
        $livre = $lr->findBy(["id"=>$id])[0];
        $lesauteurs = $ar->findAll();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod("POST")){
            $titre = $request->request->get('titre');
            $auteur = $request->request->get('auteur');
                if ($titre != "") $livre->setTitre($titre);
                if ($auteur != "") $livre->setAuteur($ar->find($auteur));
            $em->persist($livre);
            $em->flush();

            return $this->redirectToRoute('livre/index');
        }

        return $this->render('livre/update.html.twig', [
            'livre' => $livre,
            'auteurs' => $lesauteurs,
            "error_message" => ""
        ]);
    }

    /**
    * @Route("/delete/{id}", name="livre/delete", methods="GET|POST")
    */
    public function delete(Request $request, LivreRepository $lr, $id)
    {
        $livre = $lr->findBy(["id"=>$id])[0];
        $em = $this->getDoctrine()->getManager();
        $em->remove($livre);
        $em->flush();
        return new Response("livre supprimé");
    }

    /**
    * @Route("/auteur/{id}", name="livre/auteur", methods="GET")
    */
    public function getLivreByAuteur(LivreRepository $lr, $id)
    {
        $livres = $lr->findBy(array("auteur"=>$id));

        if (sizeof($livres) == 0) {
            return new Response("aucun livre");
        }
        else {
            $response = new Response();
            $data = [];
            foreach ($livres as $livre){
                array_push($data, $livre->getTitre());
            }
            $response->setContent(json_encode($data));
            return $response;
        }
    }
}
