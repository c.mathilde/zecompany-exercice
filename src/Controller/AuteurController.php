<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AuteurRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Auteur;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/auteur")
 */
class AuteurController extends AbstractController
{
    /**
    * @Route("/", name="auteur/index", methods="GET|POST")
    */
    public function index(AuteurRepository $ar)
    {
        $response = new Response();
        $auteurs = $ar->findAll();
        $data = [];
        foreach ($auteurs as $auteur){
            $subdata = ["id" => $auteur->getId(),
                        "nom" => $auteur->getNom(),
                        "prenom" => $auteur->getPrenom(),
                    ];

            array_push($data, $subdata);
        }
        $response->setContent(json_encode($data));
        return $response;
    }


    /**
    * @Route("/update/{id}", name="auteur/update", methods="GET|POST")
    */
    public function update(Request $request, AuteurRepository $ar, $id)
    {
        $auteur = $ar->findBy(["id"=>$id])[0];
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod("POST")){
            $prenom = $request->request->get('prenom');
            $nom = $request->request->get('nom');
                if ($prenom != "") $auteur->setPrenom($prenom);
                if ($nom != "") $auteur->setNom($nom);
            $em->persist($auteur);
            $em->flush();
            

            return new Response("auteur modifié");
        }
        return new Response("route modification auteur");
    }

    /**
    * @Route("/delete/{id}", name="auteur/delete", methods="GET|POST")
    */
    public function delete(Request $request, AuteurRepository $ar, $id)
    {
        $auteur = $ar->findBy(["id"=>$id])[0];
        $em = $this->getDoctrine()->getManager();
        $em->remove($auteur);
        $em->flush();
        return new Response("auteur supprimé");
    }

    /**
    * @Route("/add", name="auteur/add", methods="GET|POST")
    */
    public function add(Request $request, AuteurRepository $ar)
    {
        $auteur = new Auteur();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod("POST")){
            $prenom = $request->request->get('prenom');
            $nom = $request->request->get('nom');
                if ($prenom != "") $auteur->setPrenom($prenom);
                if ($nom != "") $auteur->setNom($nom);
            $em->persist($auteur);
            $em->flush();
            

            return new Response("auteur ajouté");
        }
        return new Response("route ajout auteur");
    }

}