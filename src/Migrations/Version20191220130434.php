<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191220130434 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE REALISE DROP FOREIGN KEY realise_ibfk_1');
        $this->addSql('ALTER TABLE REALISE DROP FOREIGN KEY realise_ibfk_2');
        $this->addSql('CREATE TABLE livre (id INT AUTO_INCREMENT NOT NULL, auteur_id INT NOT NULL, titre VARCHAR(255) NOT NULL, INDEX IDX_AC634F9960BB6FE6 (auteur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE livre_personne (livre_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_63052F4637D925CB (livre_id), INDEX IDX_63052F46A21BD112 (personne_id), PRIMARY KEY(livre_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE livre ADD CONSTRAINT FK_AC634F9960BB6FE6 FOREIGN KEY (auteur_id) REFERENCES auteur (id)');
        $this->addSql('ALTER TABLE livre_personne ADD CONSTRAINT FK_63052F4637D925CB FOREIGN KEY (livre_id) REFERENCES livre (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE livre_personne ADD CONSTRAINT FK_63052F46A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE CARNET');
        $this->addSql('DROP TABLE FILM');
        $this->addSql('DROP TABLE REALISATEUR');
        $this->addSql('DROP TABLE REALISE');
        $this->addSql('DROP TABLE utilisateur');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE livre_personne DROP FOREIGN KEY FK_63052F4637D925CB');
        $this->addSql('CREATE TABLE CARNET (ID INT AUTO_INCREMENT NOT NULL, NOM VARCHAR(30) DEFAULT NULL COLLATE utf8_general_ci, PRENOM VARCHAR(30) DEFAULT NULL COLLATE utf8_general_ci, NAISSANCE DATE DEFAULT NULL, VILLE VARCHAR(30) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE FILM (id_film INT AUTO_INCREMENT NOT NULL, titre VARCHAR(50) NOT NULL COLLATE utf8_general_ci, annee INT NOT NULL, genre VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, resume VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, code_pays VARCHAR(3) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(id_film)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE REALISATEUR (id_realis INT AUTO_INCREMENT NOT NULL, nom_realis VARCHAR(30) DEFAULT NULL COLLATE utf8_general_ci, prenom_realis VARCHAR(30) DEFAULT NULL COLLATE utf8_general_ci, anne_nais INT DEFAULT NULL, PRIMARY KEY(id_realis)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE REALISE (id_film INT NOT NULL, id_realis INT NOT NULL, INDEX id_realis (id_realis), INDEX IDX_2A892784964A220 (id_film), PRIMARY KEY(id_film, id_realis)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE utilisateur (login VARCHAR(30) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, password VARCHAR(30) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE REALISE ADD CONSTRAINT realise_ibfk_1 FOREIGN KEY (id_film) REFERENCES FILM (id_film)');
        $this->addSql('ALTER TABLE REALISE ADD CONSTRAINT realise_ibfk_2 FOREIGN KEY (id_realis) REFERENCES REALISATEUR (id_realis)');
        $this->addSql('DROP TABLE livre');
        $this->addSql('DROP TABLE livre_personne');
    }
}
