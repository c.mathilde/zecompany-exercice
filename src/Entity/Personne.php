<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonneRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *      "AUTEUR" = "App\Entity\Auteur",
 *      "CLIENT" = "App\Entity\Client"
 * })
 */
abstract class Personne
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Livre", mappedBy="lecteur")
     */
    private $livresLus;

    public function __construct()
    {
        $this->livresLus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return Collection|Livre[]
     */
    public function getLivresLus(): Collection
    {
        return $this->livresLus;
    }

    public function addLivresLus(Livre $livre): self
    {
        if (!$this->livresLus->contains($livre)) {
            $this->livresLus[] = $livre;
            $livre->addLecteur($this);
        }

        return $this;
    }

    public function removeLivresLus(Livre $livre): self
    {
        if ($this->livresLus->contains($livre)) {
            $this->livresLus->removeElement($livre);
            $livre->removeLecteur($this);
        }

        return $this;
    }
}
