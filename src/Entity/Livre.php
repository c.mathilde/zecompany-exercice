<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LivreRepository")
 */
class Livre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Auteur", inversedBy="livres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $auteur;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Personne", inversedBy="livres")
     */
    private $lecteur;

    public function __construct()
    {
        $this->lecteur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getAuteur(): ?Auteur
    {
        return $this->auteur;
    }

    public function setAuteur(?Auteur $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getLecteur(): Collection
    {
        return $this->lecteur;
    }

    public function addLecteur(Personne $lecteur): self
    {
        if (!$this->lecteur->contains($lecteur)) {
            $this->lecteur[] = $lecteur;
        }

        return $this;
    }

    public function removeLecteur(Personne $lecteur): self
    {
        if ($this->lecteur->contains($lecteur)) {
            $this->lecteur->removeElement($lecteur);
        }

        return $this;
    }
}
